TM Both
=======

Small project demonstrating use of a CMake parent project (for CLion) encompassing two CMake child projects: an application and a library.

Usage
------

Open `tmboth/tmroot/CMakeLists.txt` in CLion.
CLion should recognise and display the `tmexe` and `tmlib` projects accordingly.

Meta
----

```ini
language=c++
type=application,library,example
date=2018-07-25
```
